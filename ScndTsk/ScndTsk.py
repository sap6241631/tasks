def inputFile(file_path):
    with open(file_path, 'r') as file:
        choices = [line.strip().upper() for line in file]
    return choices

def result(choices1, choices2):
    player1_wins = 0
    player2_wins = 0
    draws = 0

    for choice1, choice2 in zip(choices1, choices2):
        if choice1 == choice2:
            draws += 1
        elif (
            (choice1 == 'R' and choice2 == 'S') or
            (choice1 == 'S' and choice2 == 'P') or
            (choice1 == 'P' and choice2 == 'R')
        ):
            player1_wins += 1
        else:
            player2_wins += 1

    return player1_wins, player2_wins, draws

def write_results(player1_wins, player2_wins, draws):
    with open('result.txt', 'w') as file:
        file.write("----------------------\n")
        file.write(f"Player1 wins: {player1_wins}\n")
        file.write(f"Player2 wins: {player2_wins}\n")
        file.write(f"Draws: {draws}\n")
        file.write("----------------------\n")

def main():
    try:
        player1_file = 'player1.txt'
        player2_file = 'player2.txt'

        choices1 = inputFile(player1_file)
        choices2 = inputFile(player2_file)

        if len(choices1) != 100 or len(choices2) != 100:
            raise ValueError("Both player files should have exactly 100 choices.")

        player1_wins, player2_wins, draws = result(choices1, choices2)

        write_results(player1_wins, player2_wins, draws)

    except ValueError as e:
        print("Error:", e)

if __name__ == "__main__":
    main()