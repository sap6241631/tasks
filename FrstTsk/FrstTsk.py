def find_min_element(lst):
    if not lst:
        raise ValueError("List must contain at least one element.")

    min_element = lst[0]
    for num in lst:
        if num < min_element:
            min_element = num

    return min_element

def find_sum_of_smallest_elements(list1, list2):
    smallest1 = find_min_element(list1)
    smallest2 = find_min_element(list2)

    return smallest1 + smallest2

if __name__ == "__main__":
    try:
        list1 = [4, 8, 2, 6, 1]
        list2 = [7, 3, 9, 5]

        result = find_sum_of_smallest_elements(list1, list2)
        print("Sum of the smallest elements:", result)
    except ValueError as e:
        print("Error:", e)